from aoc_2023_template import *


DAY = 2
PART = 2
INPUT = get_input(DAY).strip()


rules={ 'red': 12, 'green': 13, 'blue': 14 }

# Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green

def first_exercice(input):

    game_is_ok=True
    ans=0

    for line in input.split('\n'):
        game_id = int(line.split(':')[0].split(' ')[1]) 
        games = line.split(':')[1] 
        for game in games.split(';'):
            for cubes in game.strip().split(', '):
                for color in rules.keys():
                    if cubes.split(' ')[1] == color and int(cubes.split(' ')[0]) > rules[color]:
                        game_is_ok=False
        if game_is_ok:
            ans += game_id
        game_is_ok = True
    
    return ans


def second_exercice(input):

    ans=0
    
    for line in input.split('\n'):

        red,green,blue=[],[],[]
        games = line.split(':')[1] 

        for game in games.split(';'):
            for cubes in game.strip().split(', '):
                
                if cubes.split(' ')[1]=='green':
                    green.append(int(cubes.split(' ')[0]))

                if cubes.split(' ')[1]=='red':
                    red.append(int(cubes.split(' ')[0]))

                if cubes.split(' ')[1]=='blue':
                    blue.append(int(cubes.split(' ')[0]))

        ans+=max(red)*max(green)*max(blue)
    return ans
        

ans=second_exercice(INPUT)

submit(DAY, PART, ans)

