import collections
import math

from aoc_2023_template import *


DAY = 1
PART = 2
INPUT = get_input(DAY).strip()

# Your code here

dict_number={
    '1':1,
    '2':2,
    '3':3,
    '4':4,
    '5':5,
    '6':6,
    '7':7,
    '8':8,
    '9':9,
    "one":1,
    "two":2,
    "three":3,
    "four":4,
    "five":5,
    "six":6,
    "seven":7,
    "eight":8,
    "nine":9,
}


def first_exercice():
    ans = 0
    for line in INPUT.split('\n'):
        numbers=[]
        for char in line:
            if char.isdigit():
                numbers.append(char)
        score=int(numbers[0]+numbers[-1])
        ans+=score
    
    return ans


def startwith(sub_string):
    
    for key, value in dict_number.items():
        if sub_string.startswith(key):
            return value
    return ''

def second_exercice():
    ans = 0
    for line in INPUT.split('\n'):
        l = len(line)
        for i in range(l):
            sub = startwith(line[i:])
            if sub:
                ans+=10*sub
                break

        for i in range(l):
            sub = startwith(line[-i-1:])
            if sub:
                ans+=1*sub
                break
    
    return ans

ans=second_exercice()
submit(DAY, PART, ans)
